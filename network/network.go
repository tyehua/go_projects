/*
Using knowledge of Go – specifically channels, goroutines, and networking – construct an HTTP server that will:
Requirements:
- Communicate on port 12345
- In the background, retrieve the current Unix time based on your IP
	from worldtimeapi.org every E seconds where E is Euler's Constant from the math library.
- Using a GET request to the root
	Return:
		int64: the last fetched time and start time
		uint32: amt of requests that have been made to retrieve the time.
- Each time someone makes a request, use a channel to
	Send using a go routine to a logger:
		request IP
		last fetched time
		the request time
	Log this data to a file named `logs` using this format:
		<request-ip>-<fetched-time>-<request-time>\n
Constraints:
- Use string formatting to create the log output, not concatenation.
- Do NOT fetch the time during the request, this value should already be there.
*/

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"time"
)

var logTime = time.Now().Unix()

func main() {
	go updateTimeFunc()
	http.HandleFunc("/", rootFunc)
	http.ListenAndServe(":12345", nil)
}

func updateTimeFunc() {
	for {
		var e = time.Duration(math.Trunc(math.E * 1000))
		var req, accessErr = http.Get("http://www.worldtimeapi.org/api/ip")
		var m = make(map[string]string)

		if accessErr != nil {
			fmt.Fprintf(os.Stdout, "%s\n Dropping timer", accessErr)
			return
		}

		var t, _ = ioutil.ReadAll(req.Body)
		_ = json.Unmarshal(t, &m)

		var timeStr, parseErr = time.Parse(time.RFC3339, m["datetime"])

		if parseErr == nil {
			logTime = timeStr.Unix()
			fmt.Fprintf(os.Stdout, "%d\n", logTime)
		} else {
			fmt.Fprintf(os.Stdout, "Packet Dropped at Time: %d\n", time.Now().Unix())
		}

		time.Sleep(time.Millisecond * e)
	}
}

func rootFunc(w http.ResponseWriter, req *http.Request) {
	var currTime = time.Now().Unix()
	var f, fileErr = os.OpenFile("logs.txt", os.O_RDONLY, 0644)
	var logSize = 0

	go logFunc(w, logTime, currTime, req.RemoteAddr)

	if fileErr == nil {
		var reader = bufio.NewReader(f)
		for _, eofErr := reader.ReadString('\n'); eofErr == nil; _, eofErr = reader.ReadString('\n') {
			logSize++
		}
	}
	defer f.Close()

	fmt.Fprintf(w, "<%d>-<%d>-<%d>\n", logTime, currTime, logSize)
}

func logFunc(w http.ResponseWriter, logTime int64, currTime int64, ipAddr string) {
	var f, fileErr = os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if fileErr != nil {
		fmt.Fprintf(w, "%s", fileErr)
		w.WriteHeader(http.StatusInternalServerError)
	}
	defer f.Close()

	fmt.Fprintf(f, "<%s>-<%d>-<%d>\n", ipAddr, logTime, currTime)
}
