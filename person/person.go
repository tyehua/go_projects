/*
Using the previous code bits shown, develop an HTTP server that will:

Consume Person structs via a POST request to /people
Save the data in a map
Retrieve and return the marshaled json for the appropriate person when firing a GET request against /people/{name}
Write the data out to a file and return the marshaled json for all people with a GET request to /people
*/

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
)

type Person struct {
	Name       string `json: "name"`
	Age        int    `json: "age"`
	Profession string `json: "profession"`
	HairColor  string `json: "hairColor"`
}

func main() {
	http.HandleFunc("/person", personFunc)
	http.HandleFunc("/person/", searchFunc)
	http.ListenAndServe(":8080", nil)
	http.ListenAndServe(":12345", nil)
}

func searchFunc(w http.ResponseWriter, req *http.Request) {
	var name = path.Base(req.URL.String())
	var f, err = os.OpenFile("data.Json", os.O_RDONLY, 0644)

	if err != nil {
		fmt.Fprintf(w, "%s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	defer f.Close()

	var m = make(map[string]Person)
	var data, _ = ioutil.ReadAll(f)

	_ = json.Unmarshal(data, &m)

	for key, value := range m {
		if strings.ToLower(key) == strings.ToLower(name) {
			var display, _ = json.MarshalIndent(value, "", "	")
			fmt.Fprintf(w, "%s", display)
			return
		}
	}

	fmt.Fprintf(w, "%s not found", name)
}

func personFunc(w http.ResponseWriter, req *http.Request) {
	var decoder, err = ioutil.ReadAll(req.Body)

	if err != nil {
		fmt.Fprintf(w, "%s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	switch req.Method {

	case "POST":
		var m = make(map[string]Person)
		var newPerson Person

		var f, _ = os.OpenFile("data.Json", os.O_CREATE|os.O_RDONLY, 0644)
		var data, _ = ioutil.ReadAll(f)

		_ = json.Unmarshal(data, &m)
		_ = json.Unmarshal(decoder, &newPerson)

		m[newPerson.Name] = newPerson

		f.Close()
		os.Remove("data.Json")

		f, _ = os.OpenFile("data.Json", os.O_CREATE|os.O_WRONLY, 0644)
		defer f.Close()
		var write, _ = json.MarshalIndent(m, "", "	")
		f.Write(write)
		fmt.Fprintf(w, "%s was added/updated to the list!", newPerson.Name)

	case "GET":
		var f, err = os.OpenFile("data.Json", os.O_RDONLY, 0644)

		if err != nil {
			fmt.Fprintf(w, "%s", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		defer f.Close()
		var data, _ = ioutil.ReadAll(f)
		fmt.Fprintf(w, "%s", data)
	}
}
